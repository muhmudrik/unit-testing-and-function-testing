from django.shortcuts import render
from .forms import KabarForm
from .models import Kabar

# Create your views here.
def kabar(request):
    form = KabarForm(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        #do anthing u want u dumb
        kabar=Kabar(mood=form.data['mood'])
        kabar.save()
    buatform=KabarForm()
    daftarMood=Kabar.objects.order_by('-waktu')
    return render(request, 'kabar.html', {'form':buatform, 'daftarMood':daftarMood})