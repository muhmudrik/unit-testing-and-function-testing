from django.test import TestCase, LiveServerTestCase, Client
from django.urls import resolve
from django.http import HttpRequest

import unittest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from .views import kabar
from .models import Kabar
from .forms import KabarForm

# Create your tests here.
class TestKabar(TestCase):
    def test_story6_url_ada(self):
        response=Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_ganemu_urlnya(self):
        response = Client().get('/aku-pingin-tidur/')
        self.assertEqual(response.status_code, 404)    

    def test_story6_make_fungsi_kabar(self):
        found=resolve('/')
        self.assertEqual(found.func, kabar)

    def test_konten_halaman_ada_isi(self):
        self.assertIsNotNone(kabar)
    
    def test_halaman_utama_lengkap(self):
        request = HttpRequest()
        response = kabar(request)
        html_response=response.content.decode('utf8')
        self.assertIn("Halo, apa kabar?", html_response)
        self.assertContains(response, '<form')

    def test_story6_bikin_objek(self):
        Kabar.objects.create(mood="Aku Siap")
        jumlah = Kabar.objects.all().count()
        self.assertEqual(jumlah, 1)

    def test_story6_pake_template_kabar(self):
        response=Client().get('/')
        self.assertTemplateUsed(response, 'kabar.html')

    # def test_form_dan_models_save_ke_database(self):
    #     kalimat="aku mudrik"
    #     form=KabarForm({'mood':kalimat})
    #     kabar=Kabar(mood=form.data['mood'])
    #     kabar.save()
    #     self.assertEqual(Kabar.objects.get(id=1).mood, kalimat)
    

class Story6FunctionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story6FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story6FunctionalTest, self).tearDown()

    # def test_tampilan_awal(self):
    #     selenium=self.selenium
    #     selenium.get(self.live_server_url)
    #     title = selenium.find_element_by_id('halo')

    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get(self.live_server_url)
        # find the form element
        
        form_in= selenium.find_element_by_id('id_mood')
        submit = selenium.find_element_by_id('submit')

        # Fill the form with data
        form_in.send_keys('lari dari kenyataan')

        # submitting the form
        submit.send_keys(Keys.RETURN)
        self.assertIn('lari dari kenyataan', selenium.page_source)
    