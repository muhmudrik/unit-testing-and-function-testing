from . import models
from django import forms
from datetime import datetime, date

class KabarForm(forms.Form):
    mood = forms.CharField( max_length=300, 
                            required=True, 
                            widget=forms.TextInput(attrs={'placeholder': 'runnin good', 'class':'form-control'})
                            )